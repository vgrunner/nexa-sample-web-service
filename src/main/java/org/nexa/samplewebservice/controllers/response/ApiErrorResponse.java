package org.nexa.samplewebservice.controllers.response;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * The @Data and @AllArgsConstructor annotations coming from lombok library
 * saving getters/setters/constructor boilerplate code on pojo/dto classes
 * read more: <a href="https://projectlombok.org/features/">https://projectlombok.org/features/</a>
 */

@Data
@AllArgsConstructor
public class ApiErrorResponse {

    private String code;
    private String message;
}
