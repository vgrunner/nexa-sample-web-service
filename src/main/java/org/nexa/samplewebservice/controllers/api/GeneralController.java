package org.nexa.samplewebservice.controllers.api;

import jakarta.validation.constraints.Size;
import org.nexa.samplewebservice.services.LibNexaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.nio.charset.StandardCharsets;

@RestController
@RequestMapping(path = "api/v1")
@CrossOrigin(origins = "*")
public class GeneralController {

    // DI of the LibNexaService class
    @Autowired
    private LibNexaService libNexaService;

    // example of path variable. try to run in the browser: http://localhost:12345/api/v1/encode/hello_world
    @GetMapping(path = "/encode/{val}")
    public String encode(@PathVariable String val) {
        return libNexaService.encodeValue(val.getBytes(StandardCharsets.UTF_8));
    }

    // example of request param. try to run in the browser: http://localhost:12345/api/v1/decode?text=aGVsbG9fd29ybGQ=
    // or try: http://localhost:12345/api/v1/decode to decode the default value (aGVsbG8=)
    // with default value if empty and with constraint validation of param text length
    @GetMapping(path = "/decode")
    public String decode(@RequestParam(defaultValue = "aGVsbG8=") @Size(min = 3, max = 30) String text) {
        return new String(libNexaService.decodeValue(text), StandardCharsets.UTF_8);
    }

    // read more on @GetMapping, @PostMapping, @PutMapping, @DeleteMapping: https://spring.io/guides/tutorials/rest
}
