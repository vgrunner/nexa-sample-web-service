package org.nexa.samplewebservice.controllers.api;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Path;
import org.nexa.samplewebservice.controllers.response.ApiErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    // Some example, the ResponseEntityExceptionHandler parent already handle a lot of general http exceptions types.
    // read more: https://www.baeldung.com/exception-handling-for-rest-with-spring#controlleradvice

//    @ExceptionHandler
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    protected ResponseEntity<ApiErrorResponse> handleTokenNotFoundException(TokenNotFoundException ex) {
//        return new ResponseEntity<>(new ApiErrorResponse(HttpStatus.NOT_FOUND.name(), ex.getMessage()), HttpStatus.NOT_FOUND);
//    }

    // example of constraints exception in spring boot. read more: https://spring.io/guides/gs/validating-form-input
    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<ApiErrorResponse> handleConstraintViolationException(ConstraintViolationException e) {
        List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> violation : e.getConstraintViolations()) {
            String field = "";
            for (Path.Node node : violation.getPropertyPath()) {
                field = node.getName();
            }
            errors.add("'" + field + "' " + violation.getMessage());
        }
        return new ResponseEntity<>(new ApiErrorResponse(HttpStatus.BAD_REQUEST.name(), String.join(", ", errors)), HttpStatus.BAD_REQUEST);
    }
}
