package org.nexa.samplewebservice;

import org.nexa.libnexakotlin.LibNexa;
import org.nexa.libnexakotlin.LibnexajvmKt;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SampleWebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleWebServiceApplication.class, args);
	}

	// Bean in spring boot usually use to create custom singletons (default scope) in the IoC for the DI.
	// this way we can inject those beans with @Autowired annotation in spring @Service, @Component, @Repository classes, and more.
	// read more: https://docs.spring.io/spring-framework/reference/core/beans/java/bean-annotation.html
	// see LibNexaService class for usage
	@Bean
	public LibNexa libNexa() {
		return LibnexajvmKt.initializeLibNexa("");
	}
}
