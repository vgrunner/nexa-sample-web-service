package org.nexa.samplewebservice.services;

import org.nexa.libnexakotlin.LibNexa;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The @Service annotation is also a @Bean under the hood (read on @Bean in SampleWebServiceApplication class)
 * Make it simple to create beans in the IoC and inject to other classes as part of the DI.
 * Read more: <a href="https://www.baeldung.com/spring-component-repository-service">https://www.baeldung.com/spring-component-repository-service</a>
 * See GeneralController class for usage
 */

@Service
public class LibNexaService {

    // the DI of libNexa, the @Bean we configured in SampleWebServiceApplication. singleton.
    @Autowired
    private LibNexa libNexa;

    public byte[] decodeValue(String hex) {
        return libNexa.decode64(hex);
    }

    public String encodeValue(byte[] bytes) {
        return libNexa.encode64(bytes);
    }
}
